<%-- 
    Document   : listeEmp
    Created on : 7 nov. 2022, 12:51:21
    Author     : Fanjava-P14A-V46
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String[] nom=(String[]) request.getAttribute("data");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Liste employes</h1>
        <ul>
            <% for(int i=0;i<nom.length;i++){ %>
            <li><%= nom[i] %></li>
            <% } %>
        </ul>
    </body>
</html>
