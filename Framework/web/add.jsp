<%-- 
    Document   : add
    Created on : 7 nov. 2022, 14:16:46
    Author     : Fanjava-P14A-V46
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% String data=(String) request.getAttribute("data"); %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p><%= data %></p>
    </body>
</html>
