/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.UrlMethod;
import view.ModelView;

/**
 *
 * @author Fanjava
 */
public class Employe {
    
    private String nom;
    private String prenom;
    
    @UrlMethod("add-emp")
    public ModelView add(){
        ModelView mv=new ModelView();
        mv.setData("\""+nom+" "+prenom+"\" ajoute avec succes");
        mv.setUrl("add.jsp");
        return mv;
    }
    
    @UrlMethod("select-emp")
    public ModelView liste(){
        String[] noms={"Jean","Marthe","Lia"};
        ModelView mv=new ModelView();
        mv.setData(noms);
        mv.setUrl("listeEmp.jsp");
        return mv;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
}
